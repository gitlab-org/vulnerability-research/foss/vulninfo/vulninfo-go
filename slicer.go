package vulninfo

import (
	"fmt"
	"sort"
	"strings"
)

type Direction int

const (
	FORWARD   = 1
	BACKWARDS = 2
)

func (o *Ontology) AddRelation(relation *Relation, source *VulnInfoNode, dest *VulnInfoNode) {
	o.nodes[source.Identifier] = source
	o.nodes[dest.Identifier] = dest

	orel, ok := o.outRelations[source.Identifier]
	if !ok {
		orel = []string{}
	}

	orel = append(orel, relation.Identifier)
	o.outRelations[source.Identifier] = orel

	irel, ok := o.inRelations[dest.Identifier]
	if !ok {
		irel = []string{}
	}

	irel = append(irel, relation.Identifier)
	o.inRelations[dest.Identifier] = irel

	_, ok = o.relations[relation.Identifier]
	if !ok {
		o.relations[relation.Identifier] = *relation
	}
}

func dotify(lbl string) string {
	s := strings.ReplaceAll(lbl, "-", "_")
	s = strings.ReplaceAll(s, ".", "_")
	s = strings.ReplaceAll(s, "(", "_")
	s = strings.ReplaceAll(s, ")", "_")
	s = strings.ReplaceAll(s, ":", "_")
	s = strings.ReplaceAll(s, " ", "")
	return s
}

func (o *Ontology) Dot() string {
	var sb strings.Builder
	sb.WriteString("digraph {\n")
	sb.WriteString("graph[fontname=\"helvetica\"];\nnode [fontname=\"helvetica\",shape=\"rectangle\"];\nedge[fontname = \"helvetica\"];\n")
	for _, n := range o.nodes {
		sb.WriteString(fmt.Sprintf("\tn%s[label=\"%s\"];\n", dotify(n.Identifier), n.Identifier))
	}
	for _, rel := range o.relations {
		dotsrc := dotify(rel.SourceId)
		dotdst := dotify(rel.TargetId)
		sb.WriteString(fmt.Sprintf("\tn%s -> n%s [label=\"%s\"];\n", dotsrc, dotdst, rel.Label))
	}
	sb.WriteString("}")
	return sb.String()
}

type NodeResult struct {
	Level int
	Node  string
}

type VulnInfoPair struct {
	First  NodeResult
	Second NodeResult
}

func (o *Ontology) CommonAncestors(firstnode string, secondnode string, relationKind RelationKind) ([]VulnInfoPair, error) {

	ancestors := []VulnInfoPair{}

	_, backseqfirstmap, err := o.Slice(firstnode, BACKWARDS, relationKind)
	if err != nil {
		return ancestors, err
	}

	_, backseqsecondmap, err := o.Slice(secondnode, BACKWARDS, relationKind)
	if err != nil {
		return ancestors, err
	}

	for firstid, backseqfirst := range backseqfirstmap {
		for secondid, backseqsecond := range backseqsecondmap {
			if firstid == secondid {
				nxtnxt := VulnInfoPair{
					First:  backseqfirst,
					Second: backseqsecond,
				}
				ancestors = append(ancestors, nxtnxt)
			}
		}
	}

	sort.Slice(ancestors, func(i, j int) bool {
		p1 := ancestors[i]
		p2 := ancestors[j]
		return (p1.First.Level + p1.Second.Level) < (p2.First.Level + p2.Second.Level)
	})

	return ancestors, nil
}

func (o *Ontology) Slice(criterium string, direction Direction, relationKind RelationKind) (*Ontology, map[string]NodeResult, error) {

	var relations []string
	sequence := map[string]NodeResult{}

	var err error

	ret := &Ontology{
		nodes:        map[string]*VulnInfoNode{},
		relations:    map[string]Relation{},
		inRelations:  map[string][]string{},
		outRelations: map[string][]string{},
	}

	worklist := []NodeResult{}
	critnode := NodeResult{
		Level: 0,
		Node:  criterium,
	}

	worklist = append(worklist, critnode)

	for len(worklist) > 0 {
		nxt := worklist[len(worklist)-1]
		// pop
		worklist = worklist[:len(worklist)-1]

		if direction == FORWARD {
			relations, err = o.OutRelations(nxt.Node)
		} else {
			relations, err = o.InRelations(nxt.Node)
		}

		if err != nil {
			return nil, sequence, err
		}

		for _, relationid := range relations {
			relation, err := o.Relation(relationid)
			if err != nil {
				return nil, sequence, err
			}

			if relation.Kind != relationKind {
				continue
			}

			inNode, err := o.NodeById(relation.SourceId)
			if err != nil {
				return nil, sequence, err
			}

			outNode, err := o.NodeById(relation.TargetId)
			if err != nil {
				return nil, sequence, err
			}

			ret.AddRelation(relation, inNode, outNode)

			if direction == FORWARD {
				nxtnxt := NodeResult{
					Level: nxt.Level + 1,
					Node:  outNode.Identifier,
				}
				sequence[nxtnxt.Node] = nxtnxt
				worklist = append(worklist, nxtnxt)
			} else {
				nxtnxt := NodeResult{
					Level: nxt.Level + 1,
					Node:  inNode.Identifier,
				}
				sequence[nxtnxt.Node] = nxtnxt
				worklist = append(worklist, nxtnxt)
			}
		}
	}
	return ret, sequence, nil
}
