package vulninfo

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestQuery(t *testing.T) {
	cwe78, err := VulnInfo.NodeById("cwe-78")
	require.NoError(t, err, "could not find node")

	cwe78, err = VulnInfo.CWENodeById(78)
	require.NoError(t, err, "could not find node")

	if cwe78.CWEIdentifier() != "CWE-78" {
		t.Fail()
	}

	title := `Improper Neutralization of Special Elements used in an OS Command ('OS Command Injection')`
	desc := `The product constructs all or part of an OS command using externally-influenced input from an upstream component, but it does not neutralize or incorrectly neutralizes special elements that could modify the intended OS command when it is sent to a downstream component.`

	if cwe78.Title != title {
		t.Fail()
	}

	if cwe78.Description != desc {
		t.Fail()
	}
}
